/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "CONSULTA")
@NamedQueries({
    @NamedQuery(name = "Consulta.findAll", query = "SELECT c FROM Consulta c"),
    @NamedQuery(name = "Consulta.findByConsultaId", query = "SELECT c FROM Consulta c WHERE c.consultaId = :consultaId"),
    @NamedQuery(name = "Consulta.findByDataAtendimento", query = "SELECT c FROM Consulta c WHERE c.dataAtendimento between :txtDataDe and :txtDataAte"),
    @NamedQuery(name = "Consulta.findByNomeCompletoDataAtendimento", query = "SELECT c FROM Consulta c WHERE UPPER(c.paciente.nomeCompleto) like :nomeCompleto and c.dataAtendimento between :txtDataDe and :txtDataAte"),
    @NamedQuery(name = "Consulta.findByNomeCompleto", query = "SELECT c FROM Consulta c WHERE UPPER(c.paciente.nomeCompleto) like :nomeCompleto"),
    @NamedQuery(name = "Consulta.findByDiagnostico", query = "SELECT c FROM Consulta c WHERE c.diagnostico = :diagnostico"),
    @NamedQuery(name = "Consulta.findByPedidoPedico", query = "SELECT c FROM Consulta c WHERE c.pedidoPedico = :pedidoPedico"),
    @NamedQuery(name = "Consulta.findByMedicamentos", query = "SELECT c FROM Consulta c WHERE c.medicamentos = :medicamentos"),
    @NamedQuery(name = "Consulta.findByResultados", query = "SELECT c FROM Consulta c WHERE c.resultados = :resultados")})
public class Consulta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CONSULTA_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long consultaId;
    @Basic(optional = false)
    @Column(name = "DATA_ATENDIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dataAtendimento;
    @Basic(optional = false)
    @Column(name = "DIAGNOSTICO")
    private String diagnostico;
    @Basic(optional = false)
    @Column(name = "PEDIDO_PEDICO")
    private String pedidoPedico;
    @Basic(optional = false)
    @Column(name = "MEDICAMENTOS")
    private String medicamentos;
    @Basic(optional = false)
    @Column(name = "RESULTADOS")
    private String resultados;
    @JoinColumn(name = "PACIENTE_ID", referencedColumnName = "PACIENTE_ID")
    @ManyToOne(optional = false)
    private Paciente paciente;
    @Column(name = "DATA_ALTERACAO")
    @Temporal(TemporalType.DATE)
    private Date dataAlteracao;

    public Consulta() {
    }

    public Consulta(Long consultaId) {
        this.consultaId = consultaId;
    }

    public Consulta(Long consultaId, Date dataAtendimento, String diagnostico, String pedidoPedico, String medicamentos, String resultados) {
        this.consultaId = consultaId;
        this.dataAtendimento = dataAtendimento;
        this.diagnostico = diagnostico;
        this.pedidoPedico = pedidoPedico;
        this.medicamentos = medicamentos;
        this.resultados = resultados;
    }

    public Long getConsultaId() {
        return consultaId;
    }

    public void setConsultaId(Long consultaId) {
        this.consultaId = consultaId;
    }

    public Date getDataAtendimento() {
        return dataAtendimento;
    }

    public void setDataAtendimento(Date dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getPedidoPedico() {
        return pedidoPedico;
    }

    public void setPedidoPedico(String pedidoPedico) {
        this.pedidoPedico = pedidoPedico;
    }

    public String getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(String medicamentos) {
        this.medicamentos = medicamentos;
    }

    public String getResultados() {
        return resultados;
    }

    public void setResultados(String resultados) {
        this.resultados = resultados;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consultaId != null ? consultaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.consultaId == null && other.consultaId != null) || (this.consultaId != null && !this.consultaId.equals(other.consultaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.anhembi.hospitaluam.gep.entities.Consulta[ consultaId=" + consultaId + " ]";
    }
    
}
