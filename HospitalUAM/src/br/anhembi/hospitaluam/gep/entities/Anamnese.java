/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "ANAMNESE")
@NamedQueries({
    @NamedQuery(name = "Anamnese.findAll", query = "SELECT a FROM Anamnese a"),
    @NamedQuery(name = "Anamnese.findByAnamneseId", query = "SELECT a FROM Anamnese a WHERE a.anamneseId = :anamneseId"),
    @NamedQuery(name = "Anamnese.findByNomeCompletoDataInclusao", query = "SELECT a FROM Anamnese a WHERE UPPER(a.paciente.nomeCompleto) like :nomeCompleto and a.dataInclusao between :txtDataInicio and :txtDataFim"),
    @NamedQuery(name = "Anamnese.findByNomeCompleto", query = "SELECT a FROM Anamnese a WHERE UPPER(a.paciente.nomeCompleto) like :nomeCompleto"),
    @NamedQuery(name = "Anamnese.findByTipoSanguineo", query = "SELECT a FROM Anamnese a WHERE a.tipoSanguineo = :tipoSanguineo"),
    @NamedQuery(name = "Anamnese.findByHistoricoPatologia", query = "SELECT a FROM Anamnese a WHERE a.historicoPatologia = :historicoPatologia"),
    @NamedQuery(name = "Anamnese.findByAlergias", query = "SELECT a FROM Anamnese a WHERE a.alergias = :alergias"),
    @NamedQuery(name = "Anamnese.findBySintomas", query = "SELECT a FROM Anamnese a WHERE a.sintomas = :sintomas"),
    @NamedQuery(name = "Anamnese.findByRemedios", query = "SELECT a FROM Anamnese a WHERE a.remedios = :remedios"),
    @NamedQuery(name = "Anamnese.findByAtividadesFisicas", query = "SELECT a FROM Anamnese a WHERE a.atividadesFisicas = :atividadesFisicas"),
    @NamedQuery(name = "Anamnese.findByDataInclusao", query = "SELECT a FROM Anamnese a WHERE a.dataInclusao between :txtDataInicio and :txtDataFim"),
    @NamedQuery(name = "Anamnese.findByDataAlteracao", query = "SELECT a FROM Anamnese a WHERE a.dataAlteracao = :dataAlteracao")})
public class Anamnese implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ANAMNESE_ID")
    private Long anamneseId;
    @Column(name = "TIPO_SANGUINEO")
    private String tipoSanguineo;
    @Basic(optional = false)
    @Column(name = "FUMANTE")
    private String fumante;
    @Column(name = "HISTORICO_PATOLOGIA")
    private String historicoPatologia;
    @Column(name = "ALERGIAS")
    private String alergias;
    @Column(name = "SINTOMAS")
    private String sintomas;
    @Column(name = "REMEDIOS")
    private String remedios;
    @Column(name = "ATIVIDADES_FISICAS")
    private String atividadesFisicas;
    @Basic(optional = false)
    @Column(name = "DATA_INCLUSAO")
    @Temporal(TemporalType.DATE)
    private Date dataInclusao;
    @Column(name = "DATA_ALTERACAO")
    @Temporal(TemporalType.DATE)
    private Date dataAlteracao;
    @JoinColumn(name = "CID_3", referencedColumnName = "CID_ID")
    @ManyToOne
    private Cid cid3;
    @JoinColumn(name = "CID_4", referencedColumnName = "CID_ID")
    @ManyToOne
    private Cid cid4;
    @JoinColumn(name = "CID_2", referencedColumnName = "CID_ID")
    @ManyToOne
    private Cid cid2;
    @JoinColumn(name = "CID_PRINCIPAL", referencedColumnName = "CID_ID")
    @ManyToOne(optional = false)
    private Cid cidPrincipal;
    @JoinColumn(name = "PARECER_MEDICO", referencedColumnName = "FUNCIONARIO_ID")
    @ManyToOne(optional = false)
    private Funcionario parecerMedico;
    @JoinColumn(name = "PACIENTE_ID", referencedColumnName = "PACIENTE_ID")
    @ManyToOne(optional = false)
    private Paciente paciente;

    public Anamnese() {
    }

    public Anamnese(Long anamneseId) {
        this.anamneseId = anamneseId;
    }

    public Anamnese(Long anamneseId, Date dataInclusao) {
        this.anamneseId = anamneseId;
        this.dataInclusao = dataInclusao;
    }

    public Long getAnamneseId() {
        return anamneseId;
    }

    public void setAnamneseId(Long anamneseId) {
        this.anamneseId = anamneseId;
    }

    public String getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(String tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public String getHistoricoPatologia() {
        return historicoPatologia;
    }

    public void setHistoricoPatologia(String historicoPatologia) {
        this.historicoPatologia = historicoPatologia;
    }

    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public String getRemedios() {
        return remedios;
    }

    public void setRemedios(String remedios) {
        this.remedios = remedios;
    }

    public String getAtividadesFisicas() {
        return atividadesFisicas;
    }

    public void setAtividadesFisicas(String atividadesFisicas) {
        this.atividadesFisicas = atividadesFisicas;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Cid getCid3() {
        return cid3;
    }

    public void setCid3(Cid cid3) {
        this.cid3 = cid3;
    }

    public Cid getCid4() {
        return cid4;
    }

    public void setCid4(Cid cid4) {
        this.cid4 = cid4;
    }

    public Cid getCid2() {
        return cid2;
    }

    public void setCid2(Cid cid2) {
        this.cid2 = cid2;
    }

    public Cid getCidPrincipal() {
        return cidPrincipal;
    }

    public void setCidPrincipal(Cid cidPrincipal) {
        this.cidPrincipal = cidPrincipal;
    }

    public Funcionario getParecerMedico() {
        return parecerMedico;
    }

    public void setParecerMedico(Funcionario parecerMedico) {
        this.parecerMedico = parecerMedico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public String getFumante() {
        return fumante;
    }

    public void setFumante(String fumante) {
        this.fumante = fumante;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anamneseId != null ? anamneseId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Anamnese)) {
            return false;
        }
        Anamnese other = (Anamnese) object;
        if ((this.anamneseId == null && other.anamneseId != null) || (this.anamneseId != null && !this.anamneseId.equals(other.anamneseId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.anhembi.hospitaluam.gep.entities.Anamnese[ anamneseId=" + anamneseId + " ]";
    }
    
}
