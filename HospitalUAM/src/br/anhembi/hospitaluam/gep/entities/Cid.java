/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "CID")
@NamedQueries({
    @NamedQuery(name = "Cid.findAll", query = "SELECT c FROM Cid c"),
    @NamedQuery(name = "Cid.findByCidId", query = "SELECT c FROM Cid c WHERE c.cidId = :cidId"),
    @NamedQuery(name = "Cid.findByCodigoCid", query = "SELECT c FROM Cid c WHERE c.codigoCid = :codigoCid"),
    @NamedQuery(name = "Cid.findByCodigoCidDescricaoCid", query = "SELECT c FROM Cid c WHERE c.codigoCid = :codigoCid and UPPER(c.descricaoCid) like :descricaoCid"),
    @NamedQuery(name = "Cid.findByDescricaoCid", query = "SELECT c FROM Cid c WHERE UPPER(c.descricaoCid) like :descricaoCid")})
public class Cid implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CID_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cidId;
    @Basic(optional = false)
    @Column(name = "CODIGO_CID")
    private String codigoCid;
    @Basic(optional = false)
    @Column(name = "DESCRICAO_CID")
    private String descricaoCid;


    public Cid() {
    }

    public Cid(Long cidId) {
        this.cidId = cidId;
    }

    public Cid(Long cidId, String codigoCid, String descricaoCid) {
        this.cidId = cidId;
        this.codigoCid = codigoCid;
        this.descricaoCid = descricaoCid;
    }

    public Long getCidId() {
        return cidId;
    }

    public void setCidId(Long cidId) {
        this.cidId = cidId;
    }

    public String getCodigoCid() {
        return codigoCid;
    }

    public void setCodigoCid(String codigoCid) {
        this.codigoCid = codigoCid;
    }

    public String getDescricaoCid() {
        return descricaoCid;
    }

    public void setDescricaoCid(String descricaoCid) {
        this.descricaoCid = descricaoCid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cidId != null ? cidId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cid)) {
            return false;
        }
        Cid other = (Cid) object;
        if ((this.cidId == null && other.cidId != null) || (this.cidId != null && !this.cidId.equals(other.cidId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.anhembi.hospitaluam.gep.entities.Cid[ cidId=" + cidId + " ]";
    }
    
}
