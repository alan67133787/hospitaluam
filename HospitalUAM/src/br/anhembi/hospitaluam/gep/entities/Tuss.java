/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "TUSS")
@NamedQueries({
    @NamedQuery(name = "Tuss.findAll", query = "SELECT t FROM Tuss t"),
    @NamedQuery(name = "Tuss.findByTussId", query = "SELECT t FROM Tuss t WHERE t.tussId = :tussId"),
    @NamedQuery(name = "Tuss.findByCodigoTuss", query = "SELECT t FROM Tuss t WHERE t.codigoTuss = :codigoTuss"),
    @NamedQuery(name = "Tuss.findByCodigoTussDescricaoTuss", query = "SELECT t FROM Tuss t WHERE t.codigoTuss = :codigoTuss and UPPER(t.descricaoTuss) like :descricaoTuss"),
    @NamedQuery(name = "Tuss.findByDescricaoTuss", query = "SELECT t FROM Tuss t WHERE UPPER(t.descricaoTuss) like :descricaoTuss")}
)
public class Tuss implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TUSS_ID")
    private Long tussId;
    @Basic(optional = false)
    @Column(name = "CODIGO_TUSS")
    private String codigoTuss;
    @Basic(optional = false)
    @Column(name = "DESCRICAO_TUSS")
    private String descricaoTuss;


    public Tuss() {
    }

    public Tuss(Long tussId) {
        this.tussId = tussId;
    }

    public Tuss(Long tussId, String codigoTuss, String descricaoTuss) {
        this.tussId = tussId;
        this.codigoTuss = codigoTuss;
        this.descricaoTuss = descricaoTuss;
    }

    public Long getTussId() {
        return tussId;
    }

    public void setTussId(Long tussId) {
        this.tussId = tussId;
    }

    public String getCodigoTuss() {
        return codigoTuss;
    }

    public void setCodigoTuss(String codigoTuss) {
        this.codigoTuss = codigoTuss;
    }

    public String getDescricaoTuss() {
        return descricaoTuss;
    }

    public void setDescricaoTuss(String descricaoTuss) {
        this.descricaoTuss = descricaoTuss;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tussId != null ? tussId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tuss)) {
            return false;
        }
        Tuss other = (Tuss) object;
        if ((this.tussId == null && other.tussId != null) || (this.tussId != null && !this.tussId.equals(other.tussId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.anhembi.hospitaluam.gep.entities.Tuss[ tussId=" + tussId + " ]";
    }
    
}
