/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "HISTORICO_IMAGEM")
@NamedQueries({
    @NamedQuery(name = "HistoricoImagem.findAll", query = "SELECT h FROM HistoricoImagem h"),
    @NamedQuery(name = "HistoricoImagem.findByHistoricoImagemId", query = "SELECT h FROM HistoricoImagem h WHERE h.historicoImagemId = :historicoImagemId"),
    @NamedQuery(name = "HistoricoImagem.findByNomeCompletoDataInclusao", query = "SELECT h FROM HistoricoImagem h WHERE UPPER(h.paciente.nomeCompleto) like :nomeCompleto and  h.dataInclusao between :txtDataDe and :txtDataAte"),
    @NamedQuery(name = "HistoricoImagem.findByNomeCompleto", query = "SELECT h FROM HistoricoImagem h WHERE UPPER(h.paciente.nomeCompleto) like :nomeCompleto"),
    @NamedQuery(name = "HistoricoImagem.findByDataInclusao", query = "SELECT h FROM HistoricoImagem h WHERE h.dataInclusao between :txtDataDe and :txtDataAte"),
    @NamedQuery(name = "HistoricoImagem.findByDataAlteracao", query = "SELECT h FROM HistoricoImagem h WHERE h.dataAlteracao = :dataAlteracao")})
public class HistoricoImagem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "HISTORICO_IMAGEM_ID")
    private Long historicoImagemId;
    @Basic(optional = false)
    @Lob
    @Column(name = "ARQUIVO_EXAME")
    private byte[] arquivoExame;
    @Basic(optional = false)
    @Column(name = "NOME_ARQUIVO")
    private String nomeArquivo;
    @Basic(optional = false)
    @Column(name = "DATA_INCLUSAO")
    @Temporal(TemporalType.DATE)
    private Date dataInclusao;
    @Column(name = "DATA_ALTERACAO")
    @Temporal(TemporalType.DATE)
    private Date dataAlteracao;
    @JoinColumn(name = "CID_CID_ID", referencedColumnName = "CID_ID")
    @ManyToOne(optional = false)
    private Cid cidCidId;
    @JoinColumn(name = "MEDICO_SOLICITANTE", referencedColumnName = "FUNCIONARIO_ID")
    @ManyToOne(optional = false)
    private Funcionario medicoSolicitante;
    @JoinColumn(name = "PACIENTE_ID", referencedColumnName = "PACIENTE_ID")
    @ManyToOne(optional = false)
    private Paciente paciente;
    @JoinColumn(name = "TUSS_TUSS_ID", referencedColumnName = "TUSS_ID")
    @ManyToOne(optional = false)
    private Tuss tussTussId;

    public HistoricoImagem() {
    }

    public HistoricoImagem(Long historicoImagemId) {
        this.historicoImagemId = historicoImagemId;
    }

    public HistoricoImagem(Long historicoImagemId, byte[] arquivoExame, Date dataInclusao) {
        this.historicoImagemId = historicoImagemId;
        this.arquivoExame = arquivoExame;
        this.dataInclusao = dataInclusao;
    }

    public Long getHistoricoImagemId() {
        return historicoImagemId;
    }

    public void setHistoricoImagemId(Long historicoImagemId) {
        this.historicoImagemId = historicoImagemId;
    }

    public byte[] getArquivoExame() {
        return arquivoExame;
    }

    public void setArquivoExame(byte[] arquivoExame) {
        this.arquivoExame = arquivoExame;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }
    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Cid getCidCidId() {
        return cidCidId;
    }

    public void setCidCidId(Cid cidCidId) {
        this.cidCidId = cidCidId;
    }

    public Funcionario getMedicoSolicitante() {
        return medicoSolicitante;
    }

    public void setMedicoSolicitante(Funcionario medicoSolicitante) {
        this.medicoSolicitante = medicoSolicitante;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Tuss getTussTussId() {
        return tussTussId;
    }

    public void setTussTussId(Tuss tussTussId) {
        this.tussTussId = tussTussId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historicoImagemId != null ? historicoImagemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoImagem)) {
            return false;
        }
        HistoricoImagem other = (HistoricoImagem) object;
        if ((this.historicoImagemId == null && other.historicoImagemId != null) || (this.historicoImagemId != null && !this.historicoImagemId.equals(other.historicoImagemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.anhembi.hospitaluam.gep.entities.HistoricoImagem[ historicoImagemId=" + historicoImagemId + " ]";
    }
    
}
