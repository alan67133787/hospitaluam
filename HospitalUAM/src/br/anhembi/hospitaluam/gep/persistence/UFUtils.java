package br.anhembi.hospitaluam.gep.persistence;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 * @author Jéssica Tainan de Oliveira Martins RA: 20501386
 * @author Alan Alves da Silva RA: 20486574
 * @author Bruno de Lima Zanon RA: 20431436 
 * @author Eduardo de Oliveira Matioli RA: 5101375
 * @author Giulia Dalle Lucca Freire de Carvalho RA: 20410987
 */
public class UFUtils {
    private UFUtils(){}
       
    private static UFUtils instance = null;
    
    private static ArrayList<String> listaUFs = new ArrayList<String>();

    public  ArrayList<String> getLista(){
        return listaUFs;
    }
    
    public DefaultComboBoxModel getModel(){
        
        String values[] = new String[listaUFs.size()];

        for(int i=0; i<listaUFs.size(); i++){
            values[i] = listaUFs.get(i);
        }
        
        DefaultComboBoxModel boxModel = new DefaultComboBoxModel(values);
  
        return boxModel;
    }
    
    public static UFUtils getInstance(){
        if(instance == null){
            synchronized(listaUFs){
                instance = new UFUtils();
                listaUFs.add("AC");
                listaUFs.add("AL");
                listaUFs.add("AP");
                listaUFs.add("AM");
                listaUFs.add("BA");
                listaUFs.add("CE");
                listaUFs.add("DF");
                listaUFs.add("ES");
                listaUFs.add("GO");
                listaUFs.add("MA");
                listaUFs.add("MT");
                listaUFs.add("MS");
                listaUFs.add("MG");
                listaUFs.add("PA");
                listaUFs.add("PB");
                listaUFs.add("PR");
                listaUFs.add("PE");
                listaUFs.add("PI");
                listaUFs.add("RJ");
                listaUFs.add("RN");
                listaUFs.add("RS");
                listaUFs.add("RO");
                listaUFs.add("RR");
                listaUFs.add("SC");
                listaUFs.add("SP");
                listaUFs.add("SE");
                listaUFs.add("TO"); 
            }
        }
        return instance;
    }   
}
