package br.anhembi.hospitaluam.gep.persistence;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.hsqldb.Server;

/**
 * @author Jéssica Tainan de Oliveira Martins RA: 20501386
 * @author Alan Alves da Silva RA: 20486574
 * @author Bruno de Lima Zanon RA: 20431436 
 * @author Eduardo de Oliveira Matioli RA: 5101375
 * @author Giulia Dalle Lucca Freire de Carvalho RA: 20410987
 */
public class ConfigurarAplicacaoUtils {
    
    private static ConfigurarAplicacaoUtils instance = null;

    private static Object obj = new Object();
    
    private ConfigurarAplicacaoUtils(){
    }
    
    public static ConfigurarAplicacaoUtils getInstance(){
        if(instance == null){
            synchronized(obj){
                if(instance == null){
                    instance = new ConfigurarAplicacaoUtils();
                }
            }
        }
        return instance;
    }
    
    public void checarEstruturaAplicacao() throws IOException, URISyntaxException{
        
        /*
            Captura o diretório raiz do usuário ***
            **Compatível com Windows, MacOS e Unix
        */
        //String pathApplication = "D:\\temp\\hsqldb\\";//System.getProperty("user.home") + System.getProperty("file.separator") + "LojaRoupasAppDB" + System.getProperty("file.separator") ;
        
        String pathApplication = System.getProperty("user.home") + System.getProperty("file.separator") + "HospitalUAMAppDB" + System.getProperty("file.separator") ;
        
        File file  = new File(pathApplication);

        /*
          Caso não existe o caminho da aplicação
          cria-se a estrutura e copia os arquivos padrões do banco de dados
        */
        if(!file.exists()){
            file.mkdir();
           
            InputStream schemaIS = this.getClass().getClassLoader().getResourceAsStream("META-INF/hospitaluam.log");
            Files.copy(schemaIS, Paths.get(pathApplication + "hospitaluam.log"));
            

            InputStream schemaIS2 = this.getClass().getClassLoader().getResourceAsStream("META-INF/hospitaluam.properties");
            Files.copy(schemaIS2, Paths.get(pathApplication + "hospitaluam.properties"));

            InputStream schemaIS3 = this.getClass().getClassLoader().getResourceAsStream("META-INF/hospitaluam.script");
            Files.copy(schemaIS3, Paths.get(pathApplication + "hospitaluam.script"));
            
            InputStream schemaIS4 = this.getClass().getClassLoader().getResourceAsStream("META-INF/hospitaluam.lobs");
            Files.copy(schemaIS4, Paths.get(pathApplication + "hospitaluam.lobs"));
        }
        
        /*
          Cria a instance do HSQLDB
        */
        Server hsqlServer = new Server();

        hsqlServer.setLogWriter(null);
        hsqlServer.setSilent(true);
        hsqlServer.setDatabaseName(0, "hospitaluam");
        hsqlServer.setDatabasePath(0, "file:" + pathApplication + "hospitaluam;hsqldb.write_delay=false");
        hsqlServer.start();
    }
}
