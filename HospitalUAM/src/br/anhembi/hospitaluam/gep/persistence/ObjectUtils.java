/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.persistence;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.text.JTextComponent;

/**
 * @author Jéssica Tainan de Oliveira Martins RA: 20501386
 * @author Alan Alves da Silva RA: 20486574
 * @author Bruno de Lima Zanon RA: 20431436 
 * @author Eduardo de Oliveira Matioli RA: 5101375
 * @author Giulia Dalle Lucca Freire de Carvalho RA: 20410987
 */
public class ObjectUtils {
    
    public static boolean isEmpty(Double value){
        if(value == null || value.doubleValue() < 0){
            return true;
        }
        return false;
    }
    
    public static boolean isEmpty(Long value){
        if(value == null || value.longValue() < 0){
            return true;
        }
        return false;
    }
    
    public static boolean isEmpty(Short value){
        if(value == null || value.shortValue() < 0){
            return true;
        }
        return false;
    }
    
    public static boolean isEmpty(Integer value){
        if(value == null || value.intValue() < 0){
            return true;
        }
        return false;
    }
    
    public static boolean isEmpty(Float value){
        if(value == null || value.floatValue() < 0){
            return true;
        }
        return false;
    }
    
    public static boolean isEmpty(String value){
        if((value == null) || (value.trim().equals(""))){
            return true;
        }
        return false;
    }
    
    public static boolean isEmpty(BigInteger value){
        if(value != null){
            return isEmpty(value.intValue());
        }
        return true;
    }
    
    public static boolean isEmpty(Date value){
        if(value == null){
            return true;
        }
        return false;
    }
    
    public static Double toDouble(String value){
        if(!isEmpty(value)){
            try{
                return new Double(value);
            }catch(NumberFormatException nex){
                return null;
            }
        }
        return null;
    }
    
    public static Long toLong(String value){
        if(!isEmpty(value)){
            try{
                return new Long(value);
            }catch(NumberFormatException nex){
                return null;
            }
        }
        return null;
    }
    
    public static Integer toInteger(String value){
        if(!isEmpty(value)){
            try{
                return new Integer(value);
            }catch(NumberFormatException nex){
                return null;
            }
        }
        return null;
    }
    
    public static Short toShort(String value){
        if(!isEmpty(value)){
            try{
                return new Short(value);
            }catch(NumberFormatException nex){
                return null;
            }
        }
        return null;
    }
    
    public static Date toDate(String value){
        if(!isEmpty(value)){
            try{
                return new SimpleDateFormat("dd/MM/yyyy").parse(value);
            }catch(ParseException perx){
                return null;
            }
        }
        return null;
    }
    
     public static String toDateString(Date value){
        if(!isEmpty(value)){
            return new SimpleDateFormat("dd/MM/yyyy").format(value);
        }
        return null;
    }
    
    public static String valueOf(Long obj){
        if(isEmpty(obj)){
            return "";
        }
        return obj.toString();
    }
    
    public static String valueOf(Integer obj){
        if(isEmpty(obj)){
            return "";
        }
        return obj.toString();
    }
    
    public static String valueOf(Short obj){
        if(isEmpty(obj)){
            return "";
        }
        return obj.toString();
    }
    
    public static boolean checkNumberField(java.awt.event.FocusEvent evt){
        
        JTextComponent textComp = (JTextComponent)evt.getComponent();
        
        if(!ObjectUtils.isEmpty(textComp.getText())){
            try{
                Double.parseDouble(textComp.getText());
                return true;
            }catch(NumberFormatException nex){
                JOptionPane.showMessageDialog(null, "O campo digitado deve ser preenchido apenas com números!", "Importante!", JOptionPane.ERROR_MESSAGE);
                textComp.setText("");
                textComp.requestFocusInWindow();
                
            }
        }
        return false;
    }
    
    public static Date getCurrentTruncDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
