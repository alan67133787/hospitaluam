/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.persistence;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author matrix
 */
public abstract class AbstractBO<T> {
    
    public static final String MSG_VALIDACAO_PROPRIEDADE_TEMPLATE = "O campo {0} deve ser preenchido!";
    
    public static final String MSG_VALIDACAO_PROPRIEDADE_NUMERO_TEMPLATE = "O campo {0} deve ser maior que 0(zero)!";
    
    public EntityManager getEntityManager(){
        return EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();
    }
    
    public void checkField(ArrayList<String> listOfErrors, Object property, String labelName){
        
        if(property == null){
            listOfErrors.add(new MessageFormat(MSG_VALIDACAO_PROPRIEDADE_TEMPLATE).format(new Object[]{labelName}));
        } else if(property instanceof String || property instanceof StringBuilder){
            if(ObjectUtils.isEmpty(property.toString())){
                listOfErrors.add(new MessageFormat(MSG_VALIDACAO_PROPRIEDADE_TEMPLATE).format(new Object[]{labelName}));
            }
        } else if(property instanceof Long && ObjectUtils.isEmpty((Long)property)){
            listOfErrors.add(new MessageFormat(MSG_VALIDACAO_PROPRIEDADE_NUMERO_TEMPLATE).format(new Object[]{labelName}));
        } else if(property instanceof Integer && ObjectUtils.isEmpty((Integer)property)){
            listOfErrors.add(new MessageFormat(MSG_VALIDACAO_PROPRIEDADE_NUMERO_TEMPLATE).format(new Object[]{labelName}));
        } else if(property instanceof Double && ObjectUtils.isEmpty((Double)property)){
            listOfErrors.add(new MessageFormat(MSG_VALIDACAO_PROPRIEDADE_NUMERO_TEMPLATE).format(new Object[]{labelName}));
        } else if(property instanceof Float && ObjectUtils.isEmpty((Float)property)){
            listOfErrors.add(new MessageFormat(MSG_VALIDACAO_PROPRIEDADE_NUMERO_TEMPLATE).format(new Object[]{labelName}));
        }
    }
    
    public void verificarCamposObrigatorios(ArrayList<String> listOfErrors)throws InformacoesIncompletasException{
        if(!listOfErrors.isEmpty()){
             throw new InformacoesIncompletasException("Verifique os campos obrigatórios!", listOfErrors.toArray(new String[]{}));
        }
    }
    
    public abstract T getByID(Long id);
    
    public abstract T persistirCadastro(T transientObject) throws InformacoesIncompletasException;
    
    public List<T> findAll(String namedQuery, Class clazz){
        TypedQuery<T> query  = getEntityManager().createNamedQuery(namedQuery, clazz);
        return query.getResultList();
    }
    
    public abstract List<T> findAll();
    
    public abstract List<T> findByFilters(Hashtable<String,Object> params); 
}
