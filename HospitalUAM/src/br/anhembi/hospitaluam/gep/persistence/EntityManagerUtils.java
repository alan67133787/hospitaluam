package br.anhembi.hospitaluam.gep.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Jéssica Tainan de Oliveira Martins RA: 20501386
 * @author Alan Alves da Silva RA: 20486574
 * @author Bruno de Lima Zanon RA: 20431436 
 * @author Eduardo de Oliveira Matioli RA: 5101375
 * @author Giulia Dalle Lucca Freire de Carvalho RA: 20410987
 */
public class EntityManagerUtils {
    
    private EntityManagerFactory entityManagerFactory;
    
    private static EntityManagerUtils instance = null;

    private static Object obj = new Object();
    
    private EntityManagerUtils(){
        
    }
    
    public static EntityManagerUtils getInstance(){
        if(instance == null){
            synchronized(obj){
                if(instance == null){
                    instance = new EntityManagerUtils();
                    instance.setEntityManagerFactory(Persistence.createEntityManagerFactory("HospitalUAMUnit"));
                }
            }
        }
        return instance;
    }
    
    /**
     * @return the entityManagerFactory
     */
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    /**
     * @param entityManagerFactory the entityManagerFactory to set
     */
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }
    
    
    
}
