/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.persistence;

/**
 *
 * @author matrix
 */
public class InformacoesIncompletasException extends Exception{
    
    
    public InformacoesIncompletasException(String message, String arrayOfMensagensErros[] ){
        super(message);
        this.arrayOfMensagensErros = arrayOfMensagensErros;
    }
    
    
    private String arrayOfMensagensErros[];

    /**
     * @return the arrayOfMensagensErros
     */
    public String[] getArrayOfMensagensErros() {
        return arrayOfMensagensErros;
    }

    /**
     * @param arrayOfMensagensErros the arrayOfMensagensErros to set
     */
    public void setArrayOfMensagensErros(String[] arrayOfMensagensErros) {
        this.arrayOfMensagensErros = arrayOfMensagensErros;
    }
    
    
    
}
