/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.usuario;

import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.EntityManagerUtils;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import br.anhembi.hospitaluam.gep.persistence.ObjectUtils;
import br.anhembi.hospitaluam.gep.entities.Funcionario;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author matrix
 */
public class FuncionarioBO extends AbstractBO<Funcionario>{
    
    public static final String USUARIO_NAO_ENCONTRADO_MESSAGE = "Usuário não encontrado!";
    
    public static final String SENHA_INFORMADA_NAO_CONFERE_MESSAGE = "A senha informada não confere!";
    
    public static final String USUARIO_DESABILITADO_MESSAGE = "O usuário foi desabilitado em {0}!";
    
    public static final String CRUD_MENSAGEM_CODIGO_FUNCIONARIO = "Código Cadastro Funcionário deve ser informado!";
    
    public static final String CRUD_MENSAGEM_FUNCIONARIO_BLOQUEADO = "Funcionário bloqueado, entre em contato com RH!";
    
    public static final String CRUD_MENSAGEM_USUARIO = "Nome Usuário deve ser informado!";

    public static final String CRUD_MENSAGEM_SENHA = "Senha deve ser informada!";
    
    public static final String CRUD_MENSAGEM_SENHA_CONFIRMACAO = "Confirmação de senha deve ser informada!";
    
    public static final String CRUD_MENSAGEM_SENHA_NAO_CONFERE = "Confirmação de senha não confere!";

    public static final String CRUD_MENSAGEM_PERFIL_ACESSO = "Perfil Acesso deve ser informado!";
    
    public static final String CRUD_MENSAGEM_DDD_TELEFONE = "DDD-Telefone deve ser informado!";
    
    public static final String CRUD_MENSAGEM_TELEFONE = "Telefone deve ser informado!";
    
    private static FuncionarioBO instance;
    
    private FuncionarioBO(){}
    
    public static FuncionarioBO getInstance(){
        if(instance == null){
            instance = new FuncionarioBO();
        }
        return instance;
    }
    
    public Funcionario findByLogin(String login)throws FuncionarioNaoEncontradoException{
        
        TypedQuery<Funcionario> query  = getEntityManager().createNamedQuery("Funcionario.findByLogin",Funcionario.class);
        query.setParameter("login", login);

        Funcionario admin = null;

        try{
            admin = query.getSingleResult();
        }catch(NoResultException noex){
            throw new FuncionarioNaoEncontradoException(USUARIO_NAO_ENCONTRADO_MESSAGE);
        }
        
        return admin;
    }

    public Funcionario efetuarLoginUsuario(String login, String senha) throws FuncionarioNaoEncontradoException, SenhaInvalidaException, FuncionarioDesativadoException{
    
        EntityManager et =  EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();

        Funcionario funcionario = null;
        
        try{
            Query query = et.createNamedQuery("Funcionario.findByLogin");
            query.setParameter("login", login);
            funcionario = (Funcionario)query.getSingleResult();
        }catch(NoResultException noResultException){
            throw new FuncionarioNaoEncontradoException(USUARIO_NAO_ENCONTRADO_MESSAGE);
        }
        
        if(funcionario == null){
            throw new FuncionarioNaoEncontradoException(USUARIO_NAO_ENCONTRADO_MESSAGE);
        }

        if(!funcionario.getSenha().equals(senha)){
            throw new SenhaInvalidaException(SENHA_INFORMADA_NAO_CONFERE_MESSAGE);
        }

        if(funcionario.getAtivo() != null && !"Y".equals(funcionario.getAtivo())){
            String fmtMessage = MessageFormat.format(USUARIO_DESABILITADO_MESSAGE, new Object[]{new SimpleDateFormat("dd/MM/yyyy").format(funcionario.getDataAlteracao())});
            throw new FuncionarioDesativadoException(fmtMessage);
        }
        
        return funcionario;
    }

    @Override
    public Funcionario getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(Funcionario.class, id);
    }
    
    public Funcionario getByCadastroPessoaId(Long funcionarioId) {
        EntityManager et =  getEntityManager();
        
        Query query = et.createNamedQuery("Funcionario.findByFuncionarioId");        
        query.setParameter("funcionarioId", funcionarioId);
        
        try{
            return (Funcionario)query.getSingleResult();
        }catch(NoResultException noResultException){
            return null;
        }
    }

    @Override
    public Funcionario persistirCadastro(Funcionario transientObject) throws InformacoesIncompletasException {
        
        ArrayList<String> listOfErrors = new ArrayList<String>();
        

        
        if(ObjectUtils.isEmpty(transientObject.getLogin())){
            listOfErrors.add(CRUD_MENSAGEM_USUARIO);
        }
        
        if(ObjectUtils.isEmpty(transientObject.getSenha())){
            listOfErrors.add(CRUD_MENSAGEM_SENHA);
        } else {
            if(!transientObject.getSenha().equals(transientObject.getConfirmacaoSenha())){
                listOfErrors.add(CRUD_MENSAGEM_SENHA_NAO_CONFERE);
            }
        }
        
        if(ObjectUtils.isEmpty(transientObject.getConfirmacaoSenha())){
            listOfErrors.add(CRUD_MENSAGEM_SENHA_CONFIRMACAO);
        }
        
        if(ObjectUtils.isEmpty(transientObject.getPerfilAcesso())){
            listOfErrors.add(CRUD_MENSAGEM_PERFIL_ACESSO);
        } 
        
        if(ObjectUtils.isEmpty(transientObject.getDddTelefone())){
            listOfErrors.add(CRUD_MENSAGEM_DDD_TELEFONE);
        }
        
        if(ObjectUtils.isEmpty(transientObject.getTelefone())){
            listOfErrors.add(CRUD_MENSAGEM_TELEFONE);
        }
        
        super.verificarCamposObrigatorios(listOfErrors);
        
        Funcionario entityObj = null;
        
        EntityManager et =  EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();

        EntityTransaction tx = et.getTransaction();
        
        tx.begin();
        
        if(transientObject.getFuncionarioId() != null){
            et.merge(transientObject);
        } else {
            et.persist(transientObject);
        }
        
        tx.commit();
        
        entityObj = et.find(Funcionario.class, transientObject.getFuncionarioId());
        
        et.close();
        
        return entityObj;
    }

    @Override
    public List<Funcionario> findAll() {
        return findAll("Funcionario.findAll", Funcionario.class);
    }

    @Override
    public List<Funcionario> findByFilters(Hashtable<String, Object> params) {
        Long funcionarioId = (Long)params.get("funcionarioId"); 
        
        String nomeCompleto = (String)params.get("nomeCompleto"); ;
        
        Long cpf = (Long)params.get("cpf");

        if(!ObjectUtils.isEmpty(funcionarioId)){
            Funcionario p = getByID(funcionarioId);
            
            if(p != null){
                ArrayList<Funcionario> pList  = new ArrayList<Funcionario>();
                pList.add(p);
                return  pList;
            } else {
                return new ArrayList<Funcionario>();
            }
         }
        
        if(!ObjectUtils.isEmpty(nomeCompleto) && !ObjectUtils.isEmpty(cpf)){
            Query query = getEntityManager().createNamedQuery("Funcionario.findByNomeCompletoCpf");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            query.setParameter("cpf", cpf);
            return query.getResultList();
         } else if(!ObjectUtils.isEmpty(nomeCompleto)){
            Query query = getEntityManager().createNamedQuery("Funcionario.findByNomeCompleto");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            return query.getResultList();
         } else if(!ObjectUtils.isEmpty(cpf)){
            Query query = getEntityManager().createNamedQuery("Funcionario.findByCpf");
            query.setParameter("cpf", cpf);
            return query.getResultList();
         } else {
             return findAll();
         }    }
}
