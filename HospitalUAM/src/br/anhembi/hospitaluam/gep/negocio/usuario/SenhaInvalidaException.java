/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.usuario;

/**
 *
 * @author matrix
 */
public class SenhaInvalidaException extends Exception{
    public SenhaInvalidaException(String message){
        super(message);
    }
}
