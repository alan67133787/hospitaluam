/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.tuss;

import br.anhembi.hospitaluam.gep.entities.Tuss;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author alan.alves
 */
public class TussBO extends AbstractBO<Tuss>{

    private static TussBO instance;
    
    private TussBO(){}
    
    public static TussBO getInstance(){
        if(instance == null){
            instance = new TussBO();
        }
        return instance;
    }
    
    @Override
    public Tuss getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(Tuss.class, id);   
    }

    public Tuss findByCodigoTuss(String codigoTuss){
        TypedQuery<Tuss> query  = getEntityManager().createNamedQuery("Tuss.findByCodigoTuss", Tuss.class);
        query.setParameter("codigoTuss", codigoTuss);
        List<Tuss> list = query.getResultList();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }
    
    public List<Tuss> findByDescricaoTuss(String descricaoTuss){
        TypedQuery<Tuss> query  = getEntityManager().createNamedQuery("Tuss.findByDescricaoTuss", Tuss.class);
        query.setParameter("descricaoTuss", "%" +descricaoTuss.toUpperCase()+ "%");
        return query.getResultList();
    }
    
    public List<Tuss> findByCodigoTussDescricaoTuss(String codigoTuss, String descricaoTuss){
        TypedQuery<Tuss> query  = getEntityManager().createNamedQuery("Tuss.findByCodigoTussDescricaoTuss", Tuss.class);
        query.setParameter("codigoTuss", codigoTuss);
        query.setParameter("descricaoTuss", "%" +descricaoTuss.toUpperCase()+ "%");
        return query.getResultList();
    }
    
    
    @Override
    public Tuss persistirCadastro(Tuss transientObject) throws InformacoesIncompletasException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Tuss> findAll() {
        return findAll("Tuss.findAll", Tuss.class);
    }

    @Override
    public List<Tuss> findByFilters(Hashtable<String, Object> params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
