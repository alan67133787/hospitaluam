/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.consulta;

import br.anhembi.hospitaluam.gep.entities.Consulta;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.EntityManagerUtils;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import br.anhembi.hospitaluam.gep.persistence.ObjectUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author alan.alves
 */
public class ConsultaBO extends AbstractBO<Consulta>{
    
    private static ConsultaBO instance;
    
    private ConsultaBO(){}
    
    public static ConsultaBO getInstance(){
        if(instance == null){
            instance = new ConsultaBO();
        }
        return instance;
    }

    @Override
    public Consulta getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(Consulta.class, id);
    }

    @Override
    public Consulta persistirCadastro(Consulta transientObject) throws InformacoesIncompletasException {
        ArrayList<String> listOfErrors = new ArrayList<String>();
        
        checkField(listOfErrors, transientObject, "Data Atendimento");
        
        checkField(listOfErrors, transientObject.getPaciente(), "Paciente");
        
        verificarCamposObrigatorios(listOfErrors);
        
        Consulta entityObj = null;
        
        EntityManager et =  EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();

        EntityTransaction tx = et.getTransaction();
        
        tx.begin();
        
        if(transientObject.getConsultaId() != null){
            et.merge(transientObject);
        } else {
            et.persist(transientObject);
        }
        
        tx.commit();
        
        entityObj = et.find(Consulta.class, transientObject.getConsultaId());
        
        et.close();
        
        return entityObj;
    }

    @Override
    public List<Consulta> findAll() {
        return findAll("Consulta.findAll", Consulta.class);
    }

    @Override
    public List<Consulta> findByFilters(Hashtable<String, Object> params) {

        Long consultaId = (Long)params.get("consultaId");
        
        String nomeCompleto = (String)params.get("nomeCompleto");
        
        Date txtDataDe = (Date)params.get("txtDataDe");
        
        Date txtDataAte = (Date)params.get("txtDataAte");
        
        if(!ObjectUtils.isEmpty(consultaId)){
            
            Consulta p = getByID(consultaId);
            
            if(p != null){
                ArrayList<Consulta> pList  = new ArrayList<Consulta>();
                pList.add(p);
                return  pList;
            } else {
                return new ArrayList<Consulta>();
            }
        }
        
        if(!ObjectUtils.isEmpty(nomeCompleto) && !ObjectUtils.isEmpty(txtDataDe) && !ObjectUtils.isEmpty(txtDataAte)){
            Query query = getEntityManager().createNamedQuery("Consulta.findByNomeCompletoDataAtendimento");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            query.setParameter("txtDataDe", txtDataDe);
            query.setParameter("txtDataAte", txtDataAte);
            return query.getResultList();
        } else if(!ObjectUtils.isEmpty(nomeCompleto)){
            Query query = getEntityManager().createNamedQuery("Consulta.findByNomeCompleto");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            return query.getResultList();
        } else if(!ObjectUtils.isEmpty(txtDataDe) && !ObjectUtils.isEmpty(txtDataAte)){
            Query query = getEntityManager().createNamedQuery("Consulta.findByDataAtendimento");
            query.setParameter("txtDataDe", txtDataDe);
            query.setParameter("txtDataAte", txtDataAte);
            return query.getResultList();
        } else {
            return findAll();
        }
    }
}
