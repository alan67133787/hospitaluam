/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.cid;

import br.anhembi.hospitaluam.gep.entities.Cid;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author alan.alves
 */
public class CidBO extends AbstractBO<Cid>{
    
    private static CidBO instance;
    
    private CidBO(){}
    
    public static CidBO getInstance(){
        if(instance == null){
            instance = new CidBO();
        }
        return instance;
    }

    @Override
    public Cid getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(Cid.class, id);
    }
    
    public Cid findByCodigoCid(String codigoCid){
        TypedQuery<Cid> query  = getEntityManager().createNamedQuery("Cid.findByCodigoCid", Cid.class);
        query.setParameter("codigoCid", codigoCid);
        List<Cid> list = query.getResultList();
        
        if(!list.isEmpty()){
            return list.get(0);
        }
        
        return null;
    }
    
    public List<Cid> findByDescricaoCid(String descricaoCid){
        TypedQuery<Cid> query  = getEntityManager().createNamedQuery("Cid.findByDescricaoCid", Cid.class);
        query.setParameter("descricaoCid", "%" +descricaoCid.toUpperCase()+ "%");
        return query.getResultList();
    }

    public List<Cid> findByCodigoCidDescricaoCid(String codigoCid, String descricaoCid){
        TypedQuery<Cid> query  = getEntityManager().createNamedQuery("Cid.findByCodigoCidDescricaoCid", Cid.class);
        query.setParameter("codigoCid", codigoCid);
        query.setParameter("descricaoCid", "%" +descricaoCid.toUpperCase()+ "%");
        return query.getResultList();
    }
    
    
    @Override
    public Cid persistirCadastro(Cid transientObject) throws InformacoesIncompletasException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cid> findAll() {
        return findAll("Cid.findAll", Cid.class);
    }

    @Override
    public List<Cid> findByFilters(Hashtable<String, Object> params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
