/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.paciente;

import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.entities.Paciente;
import br.anhembi.hospitaluam.gep.persistence.EntityManagerUtils;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import br.anhembi.hospitaluam.gep.persistence.ObjectUtils;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author alan.alves
 */
public class PacienteBO extends AbstractBO<Paciente>{
    
    private static PacienteBO instance;
    
    private PacienteBO(){}
    
    public static PacienteBO getInstance(){
        if(instance == null){
            instance = new PacienteBO();
        }
        return instance;
    }

    @Override
    public Paciente getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(Paciente.class, id);    }

    @Override
    public Paciente persistirCadastro(Paciente transientObject) throws InformacoesIncompletasException {

        ArrayList<String> listOfErrors = new ArrayList<String>();
        
        checkField(listOfErrors, transientObject.getNomeCompleto(), "Nome Completo");
        checkField(listOfErrors, transientObject.getDataNascimento(), "Data Nascimento");
        checkField(listOfErrors, transientObject.getNacionalidade(), "Nacionalidade");  
        checkField(listOfErrors, transientObject.getNomeMae(), "Nome Mãe");  
        
        verificarCamposObrigatorios(listOfErrors);
        
        Paciente entityObj = null;
        
        EntityManager et =  EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();

        EntityTransaction tx = et.getTransaction();
        
        tx.begin();
        
        if(transientObject.getPacienteId() != null){
            et.merge(transientObject);
        } else {
            et.persist(transientObject);
        }
        
        tx.commit();
        
        entityObj = et.find(Paciente.class, transientObject.getPacienteId());
        
        et.close();
        
        return entityObj;
    }

    @Override
    public List<Paciente> findAll() {
        return findAll("Paciente.findAll", Paciente.class);
    }

    @Override
    public List<Paciente> findByFilters(Hashtable<String, Object> params) {

        Long pacienteId = (Long)params.get("pacienteId"); 
        
        String nomeCompleto = (String)params.get("nomeCompleto"); ;
        
        Long cpf = (Long)params.get("cpf");

        if(!ObjectUtils.isEmpty(pacienteId)){
            Paciente p = getByID(pacienteId);
            
            if(p != null){
                ArrayList<Paciente> pList  = new ArrayList<Paciente>();
                pList.add(p);
                return  pList;
            } else {
                return new ArrayList<Paciente>();
            }
         }
        
        if(!ObjectUtils.isEmpty(nomeCompleto) && !ObjectUtils.isEmpty(cpf)){
            Query query = getEntityManager().createNamedQuery("Paciente.findByNomeCompletoCpf");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            query.setParameter("cpf", cpf);
            return query.getResultList();
         } else if(!ObjectUtils.isEmpty(nomeCompleto)){
            Query query = getEntityManager().createNamedQuery("Paciente.findByNomeCompleto");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            return query.getResultList();
         } else if(!ObjectUtils.isEmpty(cpf)){
            Query query = getEntityManager().createNamedQuery("Paciente.findByCpf");
            query.setParameter("cpf", cpf);
            return query.getResultList();
         } else {
             return findAll();
         }
    }
}
