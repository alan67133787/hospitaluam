/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.historicoimagem;

import br.anhembi.hospitaluam.gep.entities.HistoricoImagem;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.EntityManagerUtils;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import br.anhembi.hospitaluam.gep.persistence.ObjectUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author alan.alves
 */
public class HistoricoImagemBO extends AbstractBO<HistoricoImagem>{
    
    private static HistoricoImagemBO instance;
    
    private HistoricoImagemBO(){}
    
    public static HistoricoImagemBO getInstance(){
        if(instance == null){
            instance = new HistoricoImagemBO();
        }
        return instance;
    }

    @Override
    public HistoricoImagem getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(HistoricoImagem.class, id);
    }

    @Override
    public HistoricoImagem persistirCadastro(HistoricoImagem transientObject) throws InformacoesIncompletasException {
        
        ArrayList<String> listOfErrors = new ArrayList<String>();
        
        checkField(listOfErrors, transientObject.getPaciente(), "Paciente");
        
        checkField(listOfErrors, transientObject.getMedicoSolicitante(), "Médico Solicitante");
        
        checkField(listOfErrors, transientObject.getCidCidId(), "Código CID");
        
        checkField(listOfErrors, transientObject.getTussTussId(), "Código TUSS");
        
        checkField(listOfErrors, transientObject.getArquivoExame(), "Arquivo Exame");

        verificarCamposObrigatorios(listOfErrors);
        
        HistoricoImagem entityObj = null;
        
        EntityManager et =  EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();

        EntityTransaction tx = et.getTransaction();
        
        tx.begin();
        
        if(transientObject.getHistoricoImagemId() != null){
            et.merge(transientObject);
        } else {
            et.persist(transientObject);
        }
        
        tx.commit();
        
        entityObj = et.find(HistoricoImagem.class, transientObject.getHistoricoImagemId());
        
        et.close();
        
        return entityObj;
    }

    @Override
    public List<HistoricoImagem> findAll() {
        return findAll("HistoricoImagem.findAll", HistoricoImagem.class);
    }

    @Override
    public List<HistoricoImagem> findByFilters(Hashtable<String, Object> params) {
        Long historicoImagemId = (Long)params.get("historicoImagemId");
        
        String nomeCompleto = (String)params.get("nomeCompleto");
        
        Date txtDataDe = (Date)params.get("txtDataDe");
        
        Date txtDataAte = (Date)params.get("txtDataAte");
        
        if(!ObjectUtils.isEmpty(historicoImagemId)){
            
            HistoricoImagem p = getByID(historicoImagemId);
            
            if(p != null){
                ArrayList<HistoricoImagem> pList  = new ArrayList<HistoricoImagem>();
                pList.add(p);
                return  pList;
            } else {
                return new ArrayList<HistoricoImagem>();
            }
        }
        
        if(!ObjectUtils.isEmpty(nomeCompleto) && !ObjectUtils.isEmpty(txtDataDe) && !ObjectUtils.isEmpty(txtDataAte)){
            Query query = getEntityManager().createNamedQuery("HistoricoImagem.findByNomeCompletoDataInclusao");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            query.setParameter("txtDataDe", txtDataDe);
            query.setParameter("txtDataAte", txtDataAte);
            return query.getResultList();
        } else if(!ObjectUtils.isEmpty(nomeCompleto)){
            Query query = getEntityManager().createNamedQuery("HistoricoImagem.findByNomeCompleto");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            return query.getResultList();
        } else if(!ObjectUtils.isEmpty(txtDataDe) && !ObjectUtils.isEmpty(txtDataAte)){
            Query query = getEntityManager().createNamedQuery("HistoricoImagem.findByDataInclusao");
            query.setParameter("txtDataDe", txtDataDe);
            query.setParameter("txtDataAte", txtDataAte);
            return query.getResultList();
        } else {
            return findAll();
        }
    }
}
