/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.negocio.anamnese;

import br.anhembi.hospitaluam.gep.entities.Anamnese;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.EntityManagerUtils;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import br.anhembi.hospitaluam.gep.persistence.ObjectUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author alan.alves
 */
public class AnamneseBO extends AbstractBO<Anamnese>{
    
    private static AnamneseBO instance;
    
    private AnamneseBO(){}
    
    public static AnamneseBO getInstance(){
        if(instance == null){
            instance = new AnamneseBO();
        }
        return instance;
    }

    @Override
    public Anamnese getByID(Long id) {
        EntityManager et =  getEntityManager();
        return et.find(Anamnese.class, id);
    }

    @Override
    public Anamnese persistirCadastro(Anamnese transientObject) throws InformacoesIncompletasException {
        
        ArrayList<String> listOfErrors = new ArrayList<String>();
        
        checkField(listOfErrors, transientObject.getCidPrincipal(), "CID Principal");
        
        checkField(listOfErrors, transientObject.getFumante(), "Fumante");
        
        checkField(listOfErrors, transientObject.getPaciente(), "Paciente");
        
        checkField(listOfErrors, transientObject.getParecerMedico(), "Médico");
        
        verificarCamposObrigatorios(listOfErrors);
        
        Anamnese entityObj = null;
        
        EntityManager et =  EntityManagerUtils.getInstance().getEntityManagerFactory().createEntityManager();

        EntityTransaction tx = et.getTransaction();
        
        tx.begin();
        
        if(transientObject.getAnamneseId() != null){
            et.merge(transientObject);
        } else {
            et.persist(transientObject);
        }
        
        tx.commit();
        
        entityObj = et.find(Anamnese.class, transientObject.getAnamneseId());
        
        et.close();
        
        return entityObj;
    }

    @Override
    public List<Anamnese> findAll() {
        return findAll("Anamnese.findAll", Anamnese.class);
    }

    @Override
    public List<Anamnese> findByFilters(Hashtable<String, Object> params) {
        
        Long anamneseId = (Long)params.get("anamneseId");
        
        String nomeCompleto = (String)params.get("nomeCompleto");
        
        Date txtDataInicio = (Date)params.get("txtDataInicio");
        
        Date txtDataFim = (Date)params.get("txtDataFim");
        
        if(!ObjectUtils.isEmpty(anamneseId)){
            
            Anamnese p = getByID(anamneseId);
            
            if(p != null){
                ArrayList<Anamnese> pList  = new ArrayList<Anamnese>();
                pList.add(p);
                return  pList;
            } else {
                return new ArrayList<Anamnese>();
            }
        }
        
        if(!ObjectUtils.isEmpty(nomeCompleto) && !ObjectUtils.isEmpty(txtDataInicio) && !ObjectUtils.isEmpty(txtDataFim)){
            Query query = getEntityManager().createNamedQuery("Anamnese.findByNomeCompletoDataInclusao");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            query.setParameter("txtDataInicio", txtDataInicio);
            query.setParameter("txtDataFim", txtDataFim);
            return query.getResultList();
        } else if(!ObjectUtils.isEmpty(nomeCompleto)){
            Query query = getEntityManager().createNamedQuery("Anamnese.findByNomeCompleto");
            query.setParameter("nomeCompleto", "%"+nomeCompleto.toUpperCase()+"%");
            return query.getResultList();
        } else if(!ObjectUtils.isEmpty(txtDataInicio) && !ObjectUtils.isEmpty(txtDataFim)){
            Query query = getEntityManager().createNamedQuery("Anamnese.findByDataInclusao");
            query.setParameter("txtDataInicio", txtDataInicio);
            query.setParameter("txtDataFim", txtDataFim);
            return query.getResultList();
        } else {
            return findAll();
        }
    }
}
