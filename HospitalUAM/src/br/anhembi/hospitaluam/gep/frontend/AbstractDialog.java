/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.frontend;

import br.anhembi.hospitaluam.gep.entities.Funcionario;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import br.anhembi.hospitaluam.gep.persistence.InformacoesIncompletasException;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrador
 */
public abstract class AbstractDialog<T, B extends AbstractBO> extends javax.swing.JDialog{
    
    private T entity;

    private B businessObject;
    
    private Funcionario funcionario;
    
    public AbstractDialog(java.awt.Frame parent, boolean modal, Funcionario funcionario ){
        super(parent, modal);
        this.funcionario = funcionario;
    }

    public void actCancelar(ActionEvent evt){
    
        int result = JOptionPane.showConfirmDialog(null, "Deseja cancelar a operação? \n OBS: Todas as informações não salvas seram perdidas!", "Cancelar", JOptionPane.YES_NO_OPTION);
        
        if(result == 0){
            setEntity(null);

            popularForm();
            
            this.dispose();
        }
    }
    
    public void actSalvar(ActionEvent evt){
        try {
            boolean isInclusao = getEntity() == null;
            
            if(isInclusao){
                this.entity = createEntity();
            }
            
            popularEntidade();
                        
            setEntity((T)getBusinessObject().persistirCadastro(entity));
            
            if(isInclusao){
                JOptionPane.showMessageDialog(null, "Registro Salvo com Sucesso\n\n Código Cadastro: "+this.entity.toString()+" !", "Importante!", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso\n\n Código Cadastro: "+this.entity.toString()+" !", "Importante!", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (InformacoesIncompletasException ex) {
            StringBuilder builder = new StringBuilder();
            builder.append(ex.getMessage()+ "\n");
            for(String messages : ex.getArrayOfMensagensErros()){
                builder.append(messages + "\n");
            }
            
            JOptionPane.showMessageDialog(null, builder.toString(), "Importante!", JOptionPane.ERROR_MESSAGE);
 
            Logger.getLogger(AbstractDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public abstract void popularForm();
    
    public abstract void popularEntidade();
        
    public abstract T createEntity();
    
    public abstract  B createBO();
    
    public void beforeShowForm(){}
    
    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
        
        if(entity != null){
            popularForm();
        }
    }

    public B getBusinessObject() {
        if(businessObject == null){
            businessObject = createBO();
        }
        return businessObject;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }
    
    
}
