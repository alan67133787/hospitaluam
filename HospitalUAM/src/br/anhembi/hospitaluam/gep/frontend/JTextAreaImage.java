/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.frontend;

/**
 *
 * @author Administrador
 */
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;
 
public class JTextAreaImage extends JTextArea {
    
    private final BufferedImage bufferedImage;
    
    private final TexturePaint texturePaint;
    
    private int widthImage = 0;
    
    private int heightImage = 0;
    
    public JTextAreaImage(byte file[]) throws IOException{
        super();
        bufferedImage = ImageIO.read(new ByteArrayInputStream(file));
        

        this.widthImage = bufferedImage.getWidth();
        this.heightImage = bufferedImage.getHeight();

        Rectangle rect = new Rectangle(0, 0, this.widthImage, this.heightImage);
        texturePaint = new TexturePaint(bufferedImage, rect);
        setOpaque(false);
    }
 
  /**
   * Override the painComponent method to do our image drawing.
   */
    public void paintComponent(Graphics g){
      Graphics2D g2 = (Graphics2D) g;
      g2.setPaint(texturePaint);
      g.fillRect(0, 0, getWidth(), getHeight());
      super.paintComponent(g);
    }

    public int getWidthImage() {
        return widthImage;
    }

    public void setWidthImage(int widthImage) {
        this.widthImage = widthImage;
    }

    public int getHeightImage() {
        return heightImage;
    }

    public void setHeightImage(int heightImage) {
        this.heightImage = heightImage;
    }
}
