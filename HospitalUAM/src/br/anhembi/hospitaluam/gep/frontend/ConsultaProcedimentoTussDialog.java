/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.frontend;

import br.anhembi.hospitaluam.gep.entities.Tuss;
import br.anhembi.hospitaluam.gep.negocio.tuss.TussBO;
import br.anhembi.hospitaluam.gep.persistence.ObjectUtils;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrador
 */
public class ConsultaProcedimentoTussDialog extends javax.swing.JDialog {

    private JTextField codigoOrigem = null;
    
    private JTextField descricaoOrigem = null;
    
    
    /**
     * Creates new form ConsultaProcedimentoTussDialog
     */
    public ConsultaProcedimentoTussDialog(java.awt.Frame parent, boolean modal, JTextField codigoOrigem, JTextField descricaoOrigem) {
        super(parent, modal);
        initComponents();
        this.codigoOrigem = codigoOrigem;
        this.descricaoOrigem = descricaoOrigem;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTituloConsultaProcedimento = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lblCodigoTuss = new javax.swing.JLabel();
        txtCodigoTuss = new javax.swing.JTextField();
        lblDescricaoProcedimento = new javax.swing.JLabel();
        txtDescricaoProcedimento = new javax.swing.JTextField();
        btBuscar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProcedimentos = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblTituloConsultaProcedimento.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTituloConsultaProcedimento.setText("Consulta Procedimentos TUSS");

        lblCodigoTuss.setText("Código TUSS:");

        lblDescricaoProcedimento.setText("Descrição Procedimento:");

        btBuscar.setText("Buscar");
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });

        btCancelar.setText("Cancelar");
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        jTableProcedimentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código TUSS", "Descrição Procedimento"
            }
        ));
        jTableProcedimentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProcedimentosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableProcedimentos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblTituloConsultaProcedimento, javax.swing.GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
                .addGap(135, 135, 135))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCodigoTuss)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCodigoTuss, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblDescricaoProcedimento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescricaoProcedimento)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btBuscar)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTituloConsultaProcedimento, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCodigoTuss)
                    .addComponent(txtCodigoTuss, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescricaoProcedimento)
                    .addComponent(txtDescricaoProcedimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btBuscar)
                    .addComponent(btCancelar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        // TODO add your handling code here:
        TussBO tussBO = TussBO.getInstance();
        
        java.util.List<Tuss> lista = null;
        
        if(!ObjectUtils.isEmpty(txtCodigoTuss.getText())){
            Tuss tuss = tussBO.findByCodigoTuss(txtCodigoTuss.getText());
            lista = new ArrayList<>();
            lista.add(tuss);
        } else if(ObjectUtils.isEmpty(txtCodigoTuss.getText()) && !ObjectUtils.isEmpty(txtDescricaoProcedimento.getText())){
            lista  = tussBO.findByDescricaoTuss(txtDescricaoProcedimento.getText());
        } else if(!ObjectUtils.isEmpty(txtCodigoTuss.getText()) && !ObjectUtils.isEmpty(txtDescricaoProcedimento.getText())){
            lista = tussBO.findByCodigoTussDescricaoTuss(txtCodigoTuss.getText(), txtDescricaoProcedimento.getText());
        } else {
            lista = tussBO.findAll();
       }
        
        DefaultTableModel defaultTableModel = (DefaultTableModel)jTableProcedimentos.getModel();
        
        int totalLinhas = defaultTableModel.getRowCount();
        
        for(int i=0; i<totalLinhas; i++){
            defaultTableModel.removeRow(defaultTableModel.getRowCount()-1);
        }  
        
        if((lista!= null) && (!lista.isEmpty())){
            for(Tuss tuss : lista){
                defaultTableModel.addRow(new Object[]{
                    tuss.getCodigoTuss(),
                    tuss.getDescricaoTuss()
                });
            }
        } else {      
            JOptionPane.showMessageDialog(null, "Sem informações para exibir!", "Importante!", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btBuscarActionPerformed

    private void jTableProcedimentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProcedimentosMouseClicked
        // TODO add your handling code here:
        int row = jTableProcedimentos.rowAtPoint(evt.getPoint());
        
        String codigoTuss = jTableProcedimentos.getValueAt(row, 0).toString();
        
        String descricaoTuss = jTableProcedimentos.getValueAt(row, 1).toString();
        
        codigoOrigem.setText(codigoTuss);
        descricaoOrigem.setText(descricaoTuss);
        
        this.dispose();
    }//GEN-LAST:event_jTableProcedimentosMouseClicked

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBuscar;
    private javax.swing.JButton btCancelar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTableProcedimentos;
    private javax.swing.JLabel lblCodigoTuss;
    private javax.swing.JLabel lblDescricaoProcedimento;
    private javax.swing.JLabel lblTituloConsultaProcedimento;
    private javax.swing.JTextField txtCodigoTuss;
    private javax.swing.JTextField txtDescricaoProcedimento;
    // End of variables declaration//GEN-END:variables
}
