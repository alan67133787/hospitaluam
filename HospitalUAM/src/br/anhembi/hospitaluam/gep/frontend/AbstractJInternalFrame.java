/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.anhembi.hospitaluam.gep.frontend;

import br.anhembi.hospitaluam.gep.entities.Funcionario;
import br.anhembi.hospitaluam.gep.persistence.AbstractBO;
import java.awt.Point;
import java.awt.event.ActionEvent;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JTable;

/**
 *
 * @author matrix
 */
public abstract class AbstractJInternalFrame<T, B extends AbstractBO, D extends AbstractDialog> extends JInternalFrame{

    public AbstractJInternalFrame(String title, boolean resizable, boolean closable,
                                boolean maximizable, boolean iconifiable){
        super(title,resizable,closable,maximizable,iconifiable);
    }
            
    private D jdialog;
    
    private Funcionario funcionario;

    public abstract void popularObjetoAlteracao();
        
    public String[] getPerfilsAcesso(){
        return new String[]{"ATENDENTE","MEDICO","SUPERVISOR","ADMINISTRADOR"};
    }
    
    public abstract void buscar(ActionEvent evt);
    
    public abstract void cancelarBuscar(ActionEvent evt);
    
    public final void iniciarCadastro(ActionEvent evt){
        jdialog = getJdialog();
        jdialog.setVisible(true);
        jdialog.setFocusable(true);
        jdialog.setSize(790, 400);
        jdialog.setResizable(false);
        jdialog.setUndecorated(true);    
        jdialog.beforeShowForm();
    }
    
    public final void iniciarAlteracao(JTable table, Point eventPoint){
        jdialog = getJdialog();
        
        int row = table.rowAtPoint(eventPoint);
        
        Long codigoCadastro = new Long(table.getValueAt(row, 0).toString());

        jdialog.setEntity(getBusinessObject().getByID(codigoCadastro));
        
        jdialog.setVisible(true);

        jdialog.setFocusable(true);
        
        jdialog.setSize(790, 400);
        
        jdialog.setResizable(false);
        
        jdialog.setUndecorated(true);   
        
        jdialog.beforeShowForm();
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public abstract D getJdialog();
    
    public abstract B getBusinessObject();
}
