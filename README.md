* Version
* 1.0

### Visão Geral ###

O projeto hospital UAM tem por objetivo a atualização do processo de controle e gerenciamento de todas as informações armazenadas sobre os pacientes com portabilidade de acesso ao histórico de imagem e anamnese do mesmo. Com a implantação desse módulo o cliente poderá usufruir de forma segura e eficaz um melhor gerenciamento sobre o histórico médico de todos os pacientes a oferecer um atendimento mais eficaz para os pacientes atendidos no hospital.

Para isso, implantaremos as respectivas funcionalidades:

- Cadastro do Paciente: permite a inclusão, consulta e alteração dos dados dos pacientes e a exclusão dos cadastros duplicados ou inativos.
-- Anamnese: permite a inclusão, consulta e alteração dos dados armazenados na entrevista com o paciente e a exclusão dos cadastros duplicados.
-- Histórico de Imagem: permite a inclusão, consulta e alteração dos exames realizados e a exclusão dos exames duplicados.

Essas são algumas das funcionalidades disponibilizadas ao cliente mediante as suas reais necessidades. Ao longo do projeto detalharemos mais sobre tais funções.